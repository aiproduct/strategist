import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { TreoCardModule } from '@treo/components/card';
import { IndustryDetailComponent } from './components/industry-detail/industry-detail.component';
import { IndustryListComponent } from './components/industry-list/industry-list.component';
import { IndustryOverviewComponent } from './components/industry-overview/industry-overview.component';
import { IndustryRoutingModule } from './industry-routing.module';
import { IndustryEffects } from './state/industry.effects';
import { industryReducer } from './state/industry.reducer';

@NgModule({
    declarations: [
        IndustryOverviewComponent,
        IndustryListComponent,
        IndustryDetailComponent,
    ],
    imports: [
        CommonModule,
        TreoCardModule,
        IndustryRoutingModule,
        StoreModule.forFeature('industries', industryReducer),
        EffectsModule.forFeature([IndustryEffects]),

        MatIconModule,
        MatMenuModule,
        MatProgressBarModule,
        MatDividerModule,
        MatTabsModule,
    ]
})
export class IndustryModule {

}   
