import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IndustryModel } from '../models/industry.model';

@Injectable({
    providedIn: 'root'
})
export class IndustryService {
    public industries: IndustryModel[] = [
        {
            id: 1,
            title: 'title',
            summary: 'summary',
            image: '',
            budget: {
                expenses: 11763.34,
                expensesLimit: 20000,
                savings: 10974.12,
                savingsGoal: 250000,
                bills: 1789.22,
                billsLimit: 1000
            },
        }
    ];


    public getIndustries(): Observable<IndustryModel[]> {
        return of(this.industries);
    }

    public getIndustry(industryId: number): Observable<IndustryModel> {
        const industryOfUndefined = this.industries.find(industry => industry.id == industryId);

        if (industryOfUndefined) {
            return of(industryOfUndefined);
        }

        throw new Error(`Industry with id ${industryId} not available`);
    }
}
