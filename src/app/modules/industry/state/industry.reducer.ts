import { createFeatureSelector, createReducer, createSelector, on } from '@ngrx/store';
import { AppState } from '../../../states/app.state';
import { IndustryModel } from '../models/industry.model';
import { errorAction, loadIndustriesSuccessAction, loadIndustrySuccessAction } from './industry.actions';


export interface State extends AppState {
    industries: IndustryState;
}

export interface IndustryState {
    industries: IndustryModel[];
    currentIndustry?: IndustryModel;
    errors: string[];
}
const initialState: IndustryState = {
    industries: [],
    errors: [],
};

const getIndustryFeatureState = createFeatureSelector<IndustryState>('industries');

export const getIndustries = createSelector(
    getIndustryFeatureState,
    state => state.industries
);
export const getIndustry = createSelector(
    getIndustryFeatureState,
    state => state.currentIndustry
);

export const getErrors = createSelector(
    getIndustryFeatureState,
    state => state.errors
);

export const industryReducer = createReducer<IndustryState>(
    initialState,
    on(loadIndustriesSuccessAction, (state, action): IndustryState => {
        return {
            ...state,
            industries: action.industries,
        };
    }),
    on(loadIndustrySuccessAction, (state, action): IndustryState => {
        return {
            ...state,
            currentIndustry: action.industry,
        };
    }),
    on(errorAction, (state, action): IndustryState => {
        return {
            ...state,
            errors: action.errors,
        };
    }),
);
