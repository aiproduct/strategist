import { createAction, props } from '@ngrx/store';
import { IndustryModel } from '../models/industry.model';

export const loadIndustriesAction = createAction(
    '[Industries] load'
);
export const loadIndustriesSuccessAction = createAction(
    '[Industries] load successful',
    props<{ industries: IndustryModel[] }>()
);
export const loadIndustryAction = createAction(
    '[Industries] load industry',
    props<{ industryId: number }>()
);
export const loadIndustrySuccessAction = createAction(
    '[Industries] load industry success',
    props<{ industry: IndustryModel }>()
);
export const errorAction = createAction(
    '[Industries] error action',
    props<{ errors: string[] }>()
);
