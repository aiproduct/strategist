import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap, switchMap } from 'rxjs/operators';
import { IndustryService } from '../services/industry.service';
import { errorAction, loadIndustriesAction, loadIndustriesSuccessAction, loadIndustryAction, loadIndustrySuccessAction } from './industry.actions';


@Injectable()
export class IndustryEffects {

    public loadUseCases$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(loadIndustriesAction),
            mergeMap(() => this.industryService.getIndustries().pipe(
                map(industries => loadIndustriesSuccessAction({ industries })),
                catchError(error => of(errorAction({ errors: [error] })))
            ))
        );
    });

    public loadUseCase$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(loadIndustryAction),
            switchMap((value, index) => {
                return this.industryService.getIndustry(value.industryId).pipe(
                    map(industry => loadIndustrySuccessAction({ industry })),
                    catchError(error => of(errorAction({ errors: [error] })))
                );
            })
        );
    });

    constructor(private actions$: Actions, private industryService: IndustryService) {

    }
}

