import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndustryDetailComponent } from './components/industry-detail/industry-detail.component';
import { IndustryListComponent } from './components/industry-list/industry-list.component';

export const routes: Routes = [
    /*
    {
        path: 'dashboard',
        component: IndustryOverviewComponent,
    },
    */
    {
        path: '',
        component: IndustryListComponent,
    },
    {
        path: ':id',
        component: IndustryDetailComponent,
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [
        RouterModule,
    ]
})
export class IndustryRoutingModule {

}
