import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { IndustryModel } from '../../models/industry.model';
import { loadIndustryAction } from '../../state/industry.actions';
import { getIndustry, State } from '../../state/industry.reducer';

@Component({
    selector: 'industry',
    templateUrl: './industry-detail.component.html',
    styleUrls: [
        './industry-detail.component.scss',
    ],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class IndustryDetailComponent implements OnInit {
    public industry$: Observable<IndustryModel>;

    public constructor(
        private route: ActivatedRoute,
        private store: Store<State>
    ) {
    }

    public ngOnInit(): void {
        this.industry$ = this.store.select(getIndustry);

        this.route.params.subscribe(params => {
            this.store.dispatch(loadIndustryAction({ industryId: params.id }));
        });
    }
}
