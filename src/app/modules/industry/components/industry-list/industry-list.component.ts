import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { IndustryModel } from '../../models/industry.model';
import { loadIndustriesAction } from '../../state/industry.actions';
import { getErrors, getIndustries, State } from '../../state/industry.reducer';

@Component({
    selector: 'industry-list',
    templateUrl: './industry-list.component.html',
    styleUrls: [
        './industry-list.component.scss',
    ],
})
export class IndustryListComponent implements OnInit {
    public industries$: Observable<IndustryModel[]>;
    public errors$: Observable<string[]>;

    constructor(private store: Store<State>) {
    }

    public ngOnInit(): void {
        this.industries$ = this.store.select(getIndustries);
        this.errors$ = this.store.select(getErrors);

        this.store.dispatch(loadIndustriesAction());
    }
}
