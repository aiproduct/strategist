export class IndustryModel {
    id: number;
    title: string;
    summary: string;
    image: string;
    budget: object;
}
