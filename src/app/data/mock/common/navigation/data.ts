/* tslint:disable:max-line-length */
import { TreoNavigationItem } from '@treo/components/navigation';

export const defaultNavigation: TreoNavigationItem[] = [
    {
        id: 'radar',
        title: 'Admin',
        subtitle: 'Treo radar Kit',
        type: 'group',
        icon: 'apps',
        children: [
            {
                id: 'radar.industries',
                title: 'Industries',
                type: 'basic',
                link: '/radar/industries'
            },
        ]
    }
];
export const compactNavigation: TreoNavigationItem[] = [
    {
        id: 'radar',
        title: 'radar',
        type: 'aside',
        icon: 'apps',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    }
];
export const futuristicNavigation: TreoNavigationItem[] = [
    {
        id: 'radar.example',
        title: 'Example component',
        type: 'basic',
        icon: 'heroicons:chart-pie',
        link: '/example'
    },
    {
        id: 'radar.dummy.1',
        title: 'Dummy menu item #1',
        icon: 'heroicons:calendar',
        type: 'basic'
    },
    {
        id: 'radar.dummy.2',
        title: 'Dummy menu item #1',
        icon: 'heroicons:user-group',
        type: 'basic'
    }
];
export const horizontalNavigation: TreoNavigationItem[] = [
    {
        id: 'radar',
        title: 'Radar',
        type: 'group',
        icon: 'apps',
        children: [] // This will be filled from defaultNavigation so we don't have to manage multiple sets of the same navigation
    },
];
