import { Injectable } from '@angular/core';
import { TreoNavigationItem } from '@treo/components/navigation';
import { TreoMockApi } from '@treo/lib/mock-api/mock-api.interfaces';
import { TreoMockApiService } from '@treo/lib/mock-api/mock-api.service';
import { compactNavigation, defaultNavigation, futuristicNavigation, horizontalNavigation } from 'app/data/mock/common/navigation/data';
import { cloneDeep } from 'lodash-es';

@Injectable({
    providedIn: 'root'
})
export class NavigationMockApi implements TreoMockApi {
    // Private Readonly
    private readonly compactNavigation: TreoNavigationItem[];
    private readonly defaultNavigation: TreoNavigationItem[];
    private readonly futuristicNavigation: TreoNavigationItem[];
    private readonly horizontalNavigation: TreoNavigationItem[];

    /**
     * Constructor
     *
     * @param treoMockApiService
     */
    constructor(private treoMockApiService: TreoMockApiService) {
        // Set the data
        this.compactNavigation = compactNavigation;
        this.defaultNavigation = defaultNavigation;
        this.futuristicNavigation = futuristicNavigation;
        this.horizontalNavigation = horizontalNavigation;

        // Register the API endpoints
        this.register();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Register
     */
    register(): void {
        this.treoMockApiService.onGet('api/common/navigation').reply(() => {

            // Fill compact navigation children using the default navigation
            this.compactNavigation.forEach((compactNavItem) => {
                this.defaultNavigation.forEach((defaultNavItem) => {
                    if (defaultNavItem.id === compactNavItem.id) {
                        compactNavItem.children = cloneDeep(defaultNavItem.children);
                    }
                });
            });

            // Fill futuristic navigation children using the default navigation
            this.futuristicNavigation.forEach((futuristicNavItem) => {
                this.defaultNavigation.forEach((defaultNavItem) => {
                    if (defaultNavItem.id === futuristicNavItem.id) {
                        futuristicNavItem.children = cloneDeep(defaultNavItem.children);
                    }
                });
            });

            // Fill horizontal navigation children using the default navigation
            this.horizontalNavigation.forEach((horizontalNavItem) => {
                this.defaultNavigation.forEach((defaultNavItem) => {
                    if (defaultNavItem.id === horizontalNavItem.id) {
                        horizontalNavItem.children = cloneDeep(defaultNavItem.children);
                    }
                });
            });

            return [
                200,
                {
                    compact: cloneDeep(this.compactNavigation),
                    default: cloneDeep(this.defaultNavigation),
                    futuristic: cloneDeep(this.futuristicNavigation),
                    horizontal: cloneDeep(this.horizontalNavigation)
                }
            ];
        });
    }
}
